package ch.usi.dag.disl.test.suite.bypassmodulevisibility.instr;

public class Profiler {

    public static void before() {
        System.out.println("disl: before");
    }

    public static void after() {
        System.out.println("disl: after");
    }

}
